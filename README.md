## jsplit -- split(1) clone, Split File(s)

jsplit(1) is a clone of split(1) but adds other options.
This was created for Systems that does not have split(1)

If split(1) is present, you should use that since it is
much much faster.

Repositories:
* [gitlab repository](https://gitlab.com/jmcunx1/jsplit) (this site)
* gemini://gem.sdf.org/jmccue/computer/repoinfo/jsplit.gmi (mirror)
* gemini://sdf.org/jmccue/computer/repoinfo/jsplit.gmi (mirror)

[j\_lib2](https://gitlab.com/jmcunx1/j_lib2)
is an **optional** dependency.

[Automake](https://en.wikipedia.org/wiki/Automake)
only confuses me, but this seems to be good enough for me.

To compile:
* If "DESTDIR" is not set, will install under /usr/local
* Execute ./build.sh to create a Makefile,
execute './build.sh --help' for options
* Then make and make install
* Works on Linux, BSD and AIX

To uninstall:
* make (see compile above)
* As root: 'make uninstall' from the source directory

This is licensed using the
[ISC Licence](https://en.wikipedia.org/wiki/ISC_license).
